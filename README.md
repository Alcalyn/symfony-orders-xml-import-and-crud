# XML import, CRUD and search

Symfony application able to import a specific XML file.

Interface to create new items, edit them or search them.

## Installation

Requires php, composer, node, yarn and a database.

``` bash
git clone https://gitlab.com/Alcalyn/symfony-orders-xml-import-and-crud.git
cd symfony-orders-xml-import-and-crud/

cp .env.dist .env
# Then edit .env to match your environment

composer install
yarn install

# Run in:
php bin/console server:run
```

Then go to <http://localhost:8000/order/>

## Import XML stream

``` bash
php bin/console app:orders:import ./orders-test.xml
```

## License

This application is under [MIT License](LICENSE).
