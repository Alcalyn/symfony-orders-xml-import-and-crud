<?php

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Model\OrderSearch;

class OrderRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function findByMarketplacePriceAndDate(OrderSearch $orderSearch)
    {
        $queryBuilder = $this->createQueryBuilder('o')
            ->setMaxResults(10);

        if ($orderSearch->marketplace) {
            $queryBuilder
                ->andWhere('o.marketplace = :marketplace')
                ->setParameter('marketplace', $orderSearch->marketplace)
            ;
        }

        if ($orderSearch->amountMin) {
            $queryBuilder
                ->andWhere('o.amount >= :amountMin')
                ->setParameter('amountMin', $orderSearch->amountMin)
            ;
        }

        if ($orderSearch->amountMax) {
            $queryBuilder
                ->andWhere('o.amount <= :amountMax')
                ->setParameter('amountMax', $orderSearch->amountMax)
            ;
        }

        if ($orderSearch->purchaseDateMin) {
            $queryBuilder
                ->andWhere('o.purchaseDate >= :purchaseDateMin')
                ->setParameter('purchaseDateMin', $orderSearch->purchaseDateMin)
            ;
        }

        if ($orderSearch->purchaseDateMax) {
            $queryBuilder
                ->andWhere('o.purchaseDate <= :purchaseDateMax')
                ->setParameter('purchaseDateMax', $orderSearch->purchaseDateMax)
            ;
        }

        return $queryBuilder
            ->getQuery()
            ->getResult()
        ;
    }
}
