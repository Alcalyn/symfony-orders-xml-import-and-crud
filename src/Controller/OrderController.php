<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Order;
use App\Form\OrderType;
use App\Form\OrderSearchType;
use App\Repository\OrderRepository;
use App\Model\OrderSearch;

/**
 * @Route("/order")
 */
class OrderController extends AbstractController
{
    /**
     * @Route("/search", name="order_search", methods={"GET", "POST"})
     */
    public function search(Request $request, OrderRepository $orderRepository): Response
    {
        $orderSearch = new OrderSearch();

        $orderSearch->purchaseDateMax = \DateTime::createFromFormat('Y', '2024');

        $form = $this->createForm(OrderSearchType::class, $orderSearch);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $orders = $orderRepository->findByMarketplacePriceAndDate($orderSearch);

            return $this->render('order/search.html.twig', [
                'orders' => $orders,
                'form' => $form->createView(),
            ]);
        }

        return $this->render('order/search.html.twig', [
            'orders' => $orderRepository->findAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/", name="order_index", methods={"GET"})
     */
    public function index(OrderRepository $orderRepository): Response
    {
        return $this->render('order/index.html.twig', [
            'orders' => $orderRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="order_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $order = new Order();
        $form = $this->createForm(OrderType::class, $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($order);
            $entityManager->flush();

            return $this->redirectToRoute('order_index');
        }

        return $this->render('order/new.html.twig', [
            'order' => $order,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="order_show", methods={"GET"})
     */
    public function show(Order $order): Response
    {
        return $this->render('order/show.html.twig', [
            'order' => $order,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="order_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Order $order): Response
    {
        $form = $this->createForm(OrderType::class, $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('order_index', [
                'id' => $order->getId(),
            ]);
        }

        return $this->render('order/edit.html.twig', [
            'order' => $order,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="order_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Order $order): Response
    {
        if ($this->isCsrfTokenValid('delete'.$order->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($order);
            $entityManager->flush();
        }

        return $this->redirectToRoute('order_index');
    }
}
