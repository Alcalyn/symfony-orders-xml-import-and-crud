<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;

class OrdersImportCommand extends Command
{
    protected static $defaultName = 'app:orders:import';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(string $name = null, EntityManagerInterface $em)
    {
        parent::__construct($name);

        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setDescription('Import commands from XML.')
            ->addArgument('xml-file', InputArgument::OPTIONAL, 'Path to the XML file to import')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $filename = $input->getArgument('xml-file');

        $node = simplexml_load_file($filename);

        $count = 0;

        foreach ($node->orders->order as $orderRawData) {
            $order = new Order();
            
            $order
                ->setRefid($orderRawData->order_refid)
                ->setMarketplace($orderRawData->marketplace)
                ->setAmount(floatval($orderRawData->order_amount))
                ->setCurrency($orderRawData->order_currency)
            ;

            $purchaseDate = \DateTime::createFromFormat('Y-m-d H:i:s', "$orderRawData->order_purchase_date $orderRawData->order_purchase_heure");
            
            if ($purchaseDate instanceof \DateTimeInterface) {
                $order->setPurchaseDate($purchaseDate);
            }

            $this->em->persist($order);

            $count++;
        }

        $this->em->flush();

        $io->success("$count orders have been imported.");
    }
}
