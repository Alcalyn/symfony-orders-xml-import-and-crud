<?php

namespace App\Model;

class OrderSearch
{
    public $marketplace;

    public $amountMin;

    public $amountMax;

    public $purchaseDateMin;

    public $purchaseDateMax;
}
